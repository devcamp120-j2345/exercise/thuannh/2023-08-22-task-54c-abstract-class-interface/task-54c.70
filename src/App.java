import models.Circle;
import models.ResizableCircle;

public class App {
    public static void main(String[] args) throws Exception {
        Circle circle = new Circle(5);
        System.out.println(circle.toString()); 
        System.out.println("Perimeter: " + circle.getPerimeter()); 
        System.out.println("Area: " + circle.getArea()); 

        ResizableCircle resizableCircle = new ResizableCircle(7);
        System.out.println(resizableCircle.toString()); 
        System.out.println("Perimeter: " + resizableCircle.getPerimeter()); 
        System.out.println("Area: " + resizableCircle.getArea());

        resizableCircle.resize(50);
        System.out.println("After resizing:");
        System.out.println(resizableCircle.toString());
        System.out.println("Perimeter: " + resizableCircle.getPerimeter());
        System.out.println("Area: " + resizableCircle.getArea());
    }
}
