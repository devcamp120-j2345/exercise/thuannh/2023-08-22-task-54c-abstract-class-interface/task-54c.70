package models;

public interface Resizable {
    void resize(int percent);
}
