package models;

public interface GeometricObject {
    double getPerimeter();
    double getArea();
}
